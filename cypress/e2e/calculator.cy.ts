describe("Calculator", () => {
  it("Default value on screen", () => {
    cy.visit("/");
    cy.get("[data-cy=screen_value]").contains("0");
  });

  it("add functions", () => {
    cy.visit("/");
    cy.get("[data-cy=button]").contains("1").click();
    cy.get("[data-cy=button]").contains("+").click();
    cy.get("[data-cy=button]").contains("3").click();
    cy.get("[data-cy=button]").contains("=").click();
    cy.get("[data-cy=screen_value]").contains("4");
  });

  it("subtraction functions with negative result", () => {
    cy.visit("/");
    cy.get("[data-cy=button]").contains("1").click();
    cy.get("[data-cy=button]").contains("-").click();
    cy.get("[data-cy=button]").contains("3").click();
    cy.get("[data-cy=button]").contains("=").click();
    cy.get("[data-cy=screen_value]").contains("-2");
  });

  it("subtraction functions with negative values", () => {
    cy.visit("/");
    cy.get("[data-cy=button]").contains("-").click();
    cy.get("[data-cy=button]").contains("1").click();
    cy.get("[data-cy=button]").contains("-").click();
    cy.get("[data-cy=button]").contains("3").click();
    cy.get("[data-cy=button]").contains("=").click();
    cy.get("[data-cy=screen_value]").contains("-4");
  });

  it("subtraction functions with positive result", () => {
    cy.visit("/");
    cy.get("[data-cy=button]").contains("5").click();
    cy.get("[data-cy=button]").contains("-").click();
    cy.get("[data-cy=button]").contains("3").click();
    cy.get("[data-cy=button]").contains("=").click();
    cy.get("[data-cy=screen_value]").contains("2");
  });

  it("division function with integer result", () => {
    cy.visit("/");
    cy.get("[data-cy=button]").contains("4").click();
    cy.get("[data-cy=button]").contains("/").click();
    cy.get("[data-cy=button]").contains("2").click();
    cy.get("[data-cy=button]").contains("=").click();
    cy.get("[data-cy=screen_value]").contains("2");
  });

  it("division function with float result", () => {
    cy.visit("/");
    cy.get("[data-cy=button]").contains("3").click();
    cy.get("[data-cy=button]").contains("/").click();
    cy.get("[data-cy=button]").contains("4").click();
    cy.get("[data-cy=button]").contains("=").click();
    cy.get("[data-cy=screen_value]").contains("0.75");
  });

  it("division function with negative result", () => {
    cy.visit("/");
    cy.get("[data-cy=button]").contains("8").click();
    cy.get("[data-cy=button]").contains("/").click();
    cy.get("[data-cy=button]").contains("-").click();
    cy.get("[data-cy=button]").contains("4").click();
    cy.get("[data-cy=button]").contains("=").click();
    cy.get("[data-cy=screen_value]").contains("-2");
  });

  it("multiplication function", () => {
    cy.visit("/");
    cy.get("[data-cy=button]").contains("3").click();
    cy.get("[data-cy=button]").contains("*").click();
    cy.get("[data-cy=button]").contains("4").click();
    cy.get("[data-cy=button]").contains("=").click();
    cy.get("[data-cy=screen_value]").contains("12");
  });

  it("multiplication function with negative result", () => {
    cy.visit("/");
    cy.get("[data-cy=button]").contains("-").click();
    cy.get("[data-cy=button]").contains("3").click();
    cy.get("[data-cy=button]").contains("*").click();
    cy.get("[data-cy=button]").contains("4").click();
    cy.get("[data-cy=button]").contains("=").click();
    cy.get("[data-cy=screen_value]").contains("-12");
  });

  it("multiple operation", () => {
    cy.visit("/");
    cy.get("[data-cy=button]").contains("4").click();
    cy.get("[data-cy=button]").contains("+").click();
    cy.get("[data-cy=button]").contains("2").click();
    cy.get("[data-cy=button]").contains("*").click();
    cy.get("[data-cy=button]").contains("2").click();
    cy.get("[data-cy=button]").contains("+").click();
    cy.get("[data-cy=button]").contains("3").click();
    cy.get("[data-cy=button]").contains("/").click();
    cy.get("[data-cy=button]").contains("6").click();
    cy.get("[data-cy=button]").contains("=").click();
    cy.get("[data-cy=screen_value]").contains("8.5");
  });

  it("reset function", () => {
    cy.visit("/");
    cy.get("[data-cy=button]").contains("7").click();
    cy.get("[data-cy=button]").contains("C").click();
    cy.get("[data-cy=screen_value]").contains("0");
  });
});
